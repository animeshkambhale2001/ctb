<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<head>
  <title>Admin login</title>
</head>
<h1 class="text-danger">
        <%
        if(request.getParameter("msg") != null)
        out.print(request.getParameter("msg"));
        %>
      </h1>

<form action="login-action.jsp">
    <div class="form-group">
      <label>Username</label>
      <input type="text" class="form-control" name="username" placeholder="Enter Username" autofocus>
    </div>
    <div class="form-group">
      <label>Password</label>
      <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>