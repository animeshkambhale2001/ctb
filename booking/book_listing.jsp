<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<%@ include file="../lib/dbcon.jsp" %>
<%@ include file="../common/header.jsp" %>
<%
String sql = "select m.moviename,m.seats,sum(b.seats) as total from bookings b left join movies m on b.movie_id=m.id group by movie_id";
Statement stmt = conn.createStatement();
ResultSet rs = stmt.executeQuery(sql);

%>
<head>
    <title>Admin panel</title>
  </head>

<table class="table table-dark table-striped">
    <thead>
        <th>Movie Name</th>
        <th>Available Seats</th>
        <th>Total Seats Booked</th>
         
    </thead>
<tbody>
<%
while(rs.next()) {
	//out.println(String.format("Movie Name: %s | Seats: %s |",rs.getString("moviename"),rs.getInt("seats")));
%>
    <tr>
        <td><%=rs.getString("moviename")%></td>
        <td><%=rs.getInt("seats")%></td>
        <td><%=rs.getInt("total")%></td>
    </tr>
<%
}

%>
</tbody>
</table>
<%
String select_sql = "select b.seats,m.moviename,u.username from bookings b left join movies m on b.movie_id = m.id left join users u on b.user_id = u.id";
Statement stmt1 = conn.createStatement();
ResultSet rs1 = stmt1.executeQuery(select_sql);

%>
<table class="table table-dark table-striped">
    <thead>
        <th>User Name</th>
        <th>Movie Name</th>
        <th>Booked Seats</th>
         
    </thead>
<tbody>
<%
while(rs1.next()) {
	//out.println(String.format("Movie Name: %s | Seats: %s |",rs.getString("moviename"),rs.getInt("seats")));
%>
    <tr>
        <td><%=rs1.getString("username")%></td>
        <td><%=rs1.getString("moviename")%></td>
        <td><%=rs1.getInt("seats")%></td>
    </tr>
<%
}

%>
</tbody>
</table>