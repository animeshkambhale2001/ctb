<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<div>
    <a href="addmovie-form.jsp">Add movie</a>
</div>

<%@ include file="../lib/dbcon.jsp" %>

<%

String select_sql = "SELECT * FROM movies";
Statement stmt = conn.createStatement();
ResultSet rs = stmt.executeQuery(select_sql);

%>
<head>
    <title>Movies operations </title>
</head>
<div style="float:right">
    <button type="button" style="border:0"><a href="../logout.jsp">Logout</a></button>
</div>
    
<table class="table table-dark table-striped">
    <thead>
        <th>Movie Name</th>
        <th>Available Seats</th>
        <th>Action</th>
         
    </thead>
<tbody>
<%
while(rs.next()) {
	//out.println(String.format("Movie Name: %s | Seats: %s |",rs.getString("moviename"),rs.getInt("seats")));
%>
    <tr>
        <td><%=rs.getString("moviename")%></td>
        <td><%=rs.getInt("seats")%></td>
        <td><a href="delete.jsp?id=<%=rs.getInt("id")%>">Delete</a>
        <a href="edit-form.jsp?id=<%= rs.getInt("id")%>">edit</a>
        </td>
    </tr>
<%
}

%>
</tbody>
</table>